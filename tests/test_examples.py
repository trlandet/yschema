import os
import yschema
from utils import open_example_files, EXAMPLE_DIR


def pytest_generate_tests(metafunc):
    """
    Generate one test case for each YAML file found among the examples,
    excluding the YAML files that are named schema.yml, since those are
    the schemas which the files in the same directory are to be compared
    with to check for validity
    """
    examples = []
    for dirname in sorted(os.listdir(EXAMPLE_DIR)):
        dn = os.path.join(EXAMPLE_DIR, dirname)
        if not os.path.isdir(dn):
            continue
        for fn in sorted(os.listdir(dn)):
            if fn.endswith('.yml') and fn != 'schema.yml':
                examples.append(os.path.join(dirname, fn))
    metafunc.parametrize("example_file", examples)


def test_example_yaml_file(example_file):
    """
    Validate this yaml file which must be named valid*.yml or
    invalid*.yml and be located next to a file called schema.yml
    """
    # Should this validation succeed or not?
    base_name = os.path.basename(example_file)
    if base_name.startswith('invalid'):
        valid = False
    elif base_name.startswith('valid'):
        valid = True
    else:
        raise ValueError('File %r is not valid or invalid!' % base_name)

    # Read schema and data files
    schema, data = open_example_files(example_file)

    # Check that valid documents are valid and that invalid docs raise error
    try:
        yschema.validate(data, schema)
    except yschema.ValidationError as e:
        print('\n    %s' % str(e).replace('\n', '\n    '))
        if valid:
            raise
    else:
        if not valid:
            raise ValueError('This test should have raised ValidationError!')

    # Check that no matter what the validation status is, we can still get
    # a list of errors (possibly empty)
    errors = yschema.validate(data, schema, return_errors=True)
    assert isinstance(errors, list)
    if valid:
        assert len(errors) == 0
    else:
        assert len(errors) > 0

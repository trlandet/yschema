import sys
import os
import subprocess
from utils import EXAMPLE_DIR
import yschema


def pytest_generate_tests(metafunc):
    dirname = 'nested02'
    dn = os.path.join(EXAMPLE_DIR, dirname)
    examples = []
    for fn in sorted(os.listdir(dn)):
        if fn.endswith('.yml') and fn != 'schema.yml':
            examples.append(os.path.join(dirname, fn))
    metafunc.parametrize("example_file", examples)


def test_command_line_program(example_file):
    valid = 'invalid' not in example_file
    data_file = os.path.join(EXAMPLE_DIR, example_file)
    data_dir = os.path.dirname(data_file)
    schema_file = os.path.join(data_dir, 'schema.yml')

    cmd = [sys.executable, '-m', 'yschema', schema_file, data_file]

    status = subprocess.call(cmd)

    if valid:
        assert status == 0
    else:
        assert status > 0

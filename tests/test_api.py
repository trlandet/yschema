import os
from utils import open_example_files
import yschema


def test_list_return():
    data_file = 'nested02/invalid05.yml'
    schema, data = open_example_files(data_file)

    errors = yschema.validate(data, schema, return_errors=True)
    assert len(errors) == 7


def test_user_type():
    class AlwaysRaises:
        def validate(self, value, key):
            return ['I hate %s and %r' % (key, value)]

    types = {'float': AlwaysRaises}

    schema = {'required a': 'float'}
    data = {'a': 4.2}

    # This should work
    yschema.validate(data, schema)

    try:
        yschema.validate(data, schema, types=types)
    except yschema.ValidationError as e:
        print('\n    %s' % e)
    else:
        raise ValueError('This should not have succeeded!')

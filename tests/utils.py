import os
import yschema

THIS_DIR = os.path.abspath(os.path.dirname(__file__))
EXAMPLE_DIR = os.path.abspath(os.path.join(THIS_DIR, '..', 'examples'))


def open_example_files(example_data_file):
    # Get the full path to the document and the schema in the same directory
    data_file = os.path.join(EXAMPLE_DIR, example_data_file)
    data_dir = os.path.dirname(data_file)
    schema_file = os.path.join(data_dir, 'schema.yml')

    with open(data_file, 'rt') as f:
        data = yschema.yaml_ordered_load(f)

    with open(schema_file, 'rt') as f:
        schema = yschema.yaml_ordered_load(f)

    return schema, data

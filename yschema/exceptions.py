class ValidationError(Exception):
    pass


class SchemaError(ValidationError):
    pass
